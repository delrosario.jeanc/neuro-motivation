# Use the official Python image as the base image
FROM python:3.9-slim

RUN apt-get update && apt-get install curl tar -y


## Install GCloud
# Downloading gcloud package
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

# Installing the package
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh

# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin


COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

# Set the working directory in the container
WORKDIR /app
