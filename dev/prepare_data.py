import pandas as pd
import pickle

char_limit_for_quotes = 100
block_size = 6


quotes = pd.read_csv("data/quotes.csv", header=None, usecols=[0,1,2])
quotes.head()

quotes_list = quotes.iloc[:,0].to_list()
quotes_list[0]


print(f"Number of quotes before processing: {len(quotes_list)}")

quotes_list_reduced = [quote for quote in quotes_list if isinstance(quote, str)]

print(f"Number of quotes removing badly read: {len(quotes_list_reduced)}")




print(f"Number of quotes for model: {len([quote for quote in quotes_list_reduced if len(quote) > char_limit_for_quotes]) / len(quotes_list_reduced)}")

quotes_list_reduced = [quote for quote in quotes_list_reduced if len(quote) <= char_limit_for_quotes]

# Identify start and end of quotes
quotes_list_reduced_clean = [quote[:-1] + ' <e>' if quote[-1] == '.' else quote + ' <e>' for quote in quotes_list_reduced]
quotes_list_reduced_clean = ['<s> ' + quote for quote in quotes_list_reduced_clean]
# quotes_list_reduced_clean[:10]
quotes_list_reduced_clean = [quote.replace(',', ' ,').replace('.', ' .').lower() for quote in quotes_list_reduced_clean]



# Remove special characters from quotes
def remove_special_characters(input_string):
    
    import re

    # Define the pattern for special characters to be removed
    pattern = r"[^\w\s.,<>]"
    
    # Use regular expression to remove special characters
    output_string = re.sub(pattern, "", input_string)
    
    return output_string

quotes_list_reduced_clean_n_s = [remove_special_characters(quote) for quote in quotes_list_reduced_clean][:100000]

# Create mapping for strings to integers and back
words = list(set(' '.join(quotes_list_reduced_clean_n_s).split(sep=' ')))
wtoi = {s:i for i,s in enumerate(words)}
itow = {i:s for s,i in wtoi.items()}


# build the dataset

 # context length: how many characters do we take to predict the next one?
X, Y = [], []
for quote in quotes_list_reduced_clean_n_s:
  
  #print(w)
  context = [wtoi['<s>']] * block_size
  for word in quote.split(sep=' '):
    ix = wtoi[word]
    X.append(context)
    Y.append(ix)

    # print(' '.join(itow[i] for i in context), '--->', itow[ix])
    context = context[1:] + [ix] # crop and append

 # Combine lists X and Y into one dataframe and save them
pd.DataFrame([ix + [iy] for ix, iy in zip(X,Y)]).to_csv('data/training_data.csv', index=False)

# Save words to integers and integers to word dictionaries
with open('data/words_to_integers.pkl', 'wb') as f:
    pickle.dump(wtoi, f)

with open('data/integers_to_words.pkl', 'wb') as f:
    pickle.dump(itow, f)
