import argparse
import torch
import pickle
import torch.nn.functional as F
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument("-t", action="store", dest="training_model", type=str, help='Training model to use. Either pretrained or scratch')
results = parser.parse_args()

if results.training_model is None:
    print("Please specify a training model to use. Either pretrained or scratch")
    exit()


training_model = results.training_model


with open('data/words_to_integers.pkl', 'rb') as f:
    wtoi = pickle.load(f)

with open('data/integers_to_words.pkl', 'rb') as f:
    itow = pickle.load(f)

training_data = pd.read_csv('data/training_data.csv')

X, Y = training_data.iloc[:,:6].to_numpy(), training_data.iloc[:, 6].to_numpy()

X = torch.tensor(X)
Y = torch.tensor(Y)


class NeuralNetwork:
    def __init__(self, generator, embedding_size=1000, context_size=6):
        self.emb_size = embedding_size

        self.C = torch.randn((len(wtoi),  self.emb_size), generator=generator)
        self.W1 = torch.randn((self.emb_size * 6, 6000), generator=generator) * 0.001
        self.b1 = torch.randn(6000, generator=generator) * 0

        self.W2 = torch.randn((6000, 6000), generator=generator) * 0.001
        self.b2 = torch.randn(6000, generator=generator) * 0 

        self.W3 = torch.randn((6000, len(wtoi)), generator=generator) * 0.001
        self.b3 = torch.randn(len(wtoi), generator=generator) * 0
        self.parameters = [self.C, self.W1, self.b1, self.W2, self.b2, self.W3, self.b3]


        self.internal_counter = 0
        self.context_size=context_size
        
        lre = torch.linspace(-3, 0, 1000)
        self.lrs = 10**lre
        self.lri = []
        self.lossi = []
        self.stepi = []

        for p in self.parameters:
            p.requires_grad = True

    def forward(self, X, Y, minibatch_size=64):

        ix = torch.randint(0, X.shape[0], (minibatch_size,))

        self.emb = self.C[X[ix]] 
        h = torch.tanh(self.emb.view(-1, self.emb_size*self.context_size) @ self.W1 + self.b1) 

        h2 = torch.tanh(h @ self.W2 + self.b2)

        logits = h2 @ self.W3 + self.b3 
        self.loss = F.cross_entropy(logits, Y[ix])

        print(self.loss.item())

    def backward(self):

        for p in self.parameters:
            p.grad = None

        self.loss.backward()

        # update
        lr = 0.1 if self.internal_counter < 100000 else 0.01

        for p in self.parameters:
            p.data += -lr * p.grad

        self.stepi.append(self.internal_counter)
        self.lossi.append(self.loss.log10().item())

        self.internal_counter += 1


def train_from_scratch():
    
    g = torch.Generator().manual_seed(2147483647) # for reproducibility
    nn = NeuralNetwork(g)

    return nn


def train_from_pretrained_model(model_path='model/model.pkl'):
    
    with open(model_path, 'rb') as f:
        nn = pickle.load(f)

    return nn



if training_model == 'pretrained':
    print('Loading model from model/model.pkl file')
    nn = train_from_pretrained_model()

else:
    print('Creating model from scratch')
    nn = train_from_scratch()


for i in range(2):  
  nn.forward(X, Y)
  nn.backward()


with open('model/model.pkl', 'wb') as f:
    pickle.dump(nn, f)

