import gitlab
import os

gl = gitlab.Gitlab(private_token=os.environ['NEURO_MOTIVATION_TOKEN'])

project = gl.projects.get(46860817)

project.pipelines.create({'ref': 'create-script-to-prepare-data', 'variables': [{'key': 'TRAINNING_TYPE', 'value': 'pretrained'}]})

latest_pipeline = project.pipelines.list(get_all=False)[0]

train_job_id = [job.id for job in latest_pipeline.jobs.list() if job.name == 'train'][0]

job = project.jobs.get(train_job_id, lazy=True)
print(job)
job.play()
